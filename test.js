const chai = window.chai
const expect = window.expect

 describe('movimientosPosibles', () =>{

    it("Choca cola tocando arriba",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,6],[4,6],[4,5]]
        snake.arriba()
        expect(snake.puede_avanzar()).to.eql(true)
    })
    it("Choca cola tocando derecha",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[4,5],[4,6],[5,6]]
        snake.derecha()
        expect(snake.puede_avanzar()).to.eql(true)
    })
    it("Choca cola tocando abajo",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,6],[6,6],[6,5]]
        snake.abajo()
        expect(snake.puede_avanzar()).to.eql(true)
    })
    it("Choca cola tocando izquierda",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[6,5],[6,4],[5,4]]
        snake.izquierda()
        expect(snake.puede_avanzar()).to.eql(true)
    })
    it("Choca pared tocando arriba",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[0,5]]
        snake.arriba()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca pared tocando derecha",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,9]]
        snake.derecha()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca pared tocando abajo",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[9,5]]
        snake.abajo()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca pared tocando izquierda",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,0]]
        snake.izquierda()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca segundo bloque de cuerpo tocando arriba",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[4,5],[3,5]]
        snake.arriba()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca segundo bloque de cuerpo tocando derecha",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,6],[5,7]]
        snake.derecha()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca segundo bloque de cuerpo tocando abajo",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[6,5],[7,5]]
        snake.abajo(),
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca segundo bloque de cuerpo tocando izquierda",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,4],[5,3]]
        snake.izquierda()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca cuerpo tocando arriba",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,6],[4,6],[4,5],[4,4]]
        snake.arriba()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca cuerpo tocando derecha",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[4,5],[4,6],[5,6],[6,6]]
        snake.derecha()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca cuerpo tocando abajo",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[5,6],[6,6],[6,5],[6,4]]
        snake.abajo()
        expect(snake.puede_avanzar()).to.eql(false)
    })
    it("Choca cuerpo tocando izquierda",()=>{
        let snake = new Snake(10,10);
        snake.snake = snake.snake = [[5,5],[6,5],[6,4],[5,4],[4,4]]
        snake.izquierda()
        expect(snake.puede_avanzar()).to.eql(false)
    })
 });
